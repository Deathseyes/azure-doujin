﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{
    public float speed = 0.5f;
    public float jumpForce = 10;
    public Transform cam;

    private Animator animator;
    private Rigidbody rb;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    private void Update()
    {
        animator.SetBool("Jump", Input.GetButtonDown("Jump"));
        if (Input.GetButtonDown("Jump"))
            rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
    }
    void FixedUpdate()
    {
        Vector3 dir = Vector3.zero;
        dir.z = Input.GetAxis("Vertical");
        dir.x = Input.GetAxis("Horizontal");


        if (dir != Vector3.zero)
        {
            UpdateAnimation(dir);
            Moving(dir);
        }
        else
        {
            Quaternion q = new Quaternion();
            q.eulerAngles = new Vector3(0, cam.rotation.eulerAngles.y, 0);
            transform.rotation = q;
        }
    }

    void UpdateAnimation(Vector3 move)
    {
        animator.SetFloat("Speed", move.magnitude * 1.5f * (move.z < 0 ? -1 : 1));
        animator.SetFloat("Direction", move.magnitude);
    }

    private void Moving(Vector3 move)
    {
        Vector3 rotation = new Vector3(move.x, move.y, move.z);
        float sign = 1;
        if (move.z < 0) sign = -1;

        if (rotation.z < 0)
        {
            rotation.z *= -1;
            rotation.x *= -1;
        }

        Quaternion q = cam.rotation * Quaternion.LookRotation(rotation, Vector3.up);
        transform.rotation = q;
        transform.position += q * new Vector3(0, 0, sign*move.magnitude * speed);
    }
}
