﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TPSCamera : MonoBehaviour
{
    public Transform player;
    public Vector3 camOffset = Vector3.zero;
    public Camera tracer;

    public float zoomSensitiveRate = 0.1f;
    public float zoomFiled = 3;
    public float zoomSpeed = 0.1f;
    public float zoomScrollRate = 0.1f;

    //方向灵敏度  
    public float sensitivityX = 10F;
    public float sensitivityY = 10F;

    //上下最大视角(Y视角)  
    public float minimumY = -60F;
    public float maximumY = 60F;

    public float minCamDist = -50;
    public float maxCamDist = -10;

    public float camZoomRate = 10.0f;

    public float debugVar;


    private bool isZoomed = false;
    private Camera self;
    private float targetField = 60;
    private float targetFieldOffset = 0;
    private GameController controller;
    private GameObject lockedTarget;
    private Vector3 virtualOffset;

    // Start is called before the first frame update
    void Start()
    {
        self = GetComponent<Camera>();
        controller = GameObject.FindWithTag("GameController").GetComponent<GameController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            isZoomed = !isZoomed;
            if (isZoomed)
            {
                Ray ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));
                if (Physics.Raycast(ray, out RaycastHit hit))
                {
                    float d = (hit.point - transform.position).magnitude;
                    targetField = CalcTargetFOV(d);
                    if (targetField < 0.1f) targetField = 0.1f;
                }
                else
                {
                    targetField = zoomFiled;
                }
                targetFieldOffset = 0;
            }
            else
            {
                targetField = 60;
            }
        }

        if (Input.GetKeyDown(KeyCode.Tab) && isZoomed)
        {
            if (lockedTarget)
            {
                lockedTarget = null;
            }
            else
            {
                Ray ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2));
                if (Physics.Raycast(ray, out RaycastHit hit))
                {
                    if (hit.collider.gameObject.CompareTag("Enemy"))
                    {
                        lockedTarget = hit.collider.gameObject;
                    }
                }
            }
        }
    }

    private void FixedUpdate()
    {
        self.fieldOfView = Mathf.Lerp(self.fieldOfView, targetField + targetFieldOffset, zoomSpeed);

        if (tracer.enabled)
            return;

        if (controller.outOfControl)
            return;

        transform.position = player.position;

        float rdx = Input.GetAxis("Mouse X") * sensitivityX * (isZoomed ? zoomSensitiveRate : 1);
        float rdy = Input.GetAxis("Mouse Y") * sensitivityY * (isZoomed ? zoomSensitiveRate : 1);

        if (lockedTarget)
        {

            Vector3 virtualTarget = lockedTarget.transform.position + virtualOffset;
            virtualTarget = Quaternion.AngleAxis(rdx, transform.up) * virtualTarget;
            virtualTarget = Quaternion.AngleAxis(-rdy, transform.right) * virtualTarget;

            transform.LookAt(virtualTarget);
            virtualOffset = virtualTarget - lockedTarget.transform.position;

            float d = (transform.position - lockedTarget.transform.position).magnitude;
            debugVar = Mathf.Atan2(10, d) * Mathf.Rad2Deg;
            targetField = Mathf.Atan2(10, d) * Mathf.Rad2Deg * 2;
        }
        else
        {
            float realY = transform.localEulerAngles.x > 180 ? transform.localEulerAngles.x - 360 : transform.localEulerAngles.x;
            rdy = Mathf.Clamp(realY - rdy, minimumY, maximumY) - realY;
            transform.Rotate(Vector3.up, rdx, Space.World);
            transform.Rotate(Vector3.right, rdy, Space.Self);
        }

        

        float scroll = Input.GetAxis("Mouse ScrollWheel");
        if (isZoomed)
        {
            targetFieldOffset -= scroll * zoomScrollRate;
            if ((targetFieldOffset + targetField) < 0.1f) targetFieldOffset = 0.1f - (targetFieldOffset+targetField);
        }
        else
        {
            camOffset.z = Mathf.Clamp(camOffset.z + scroll * camZoomRate, minCamDist, maxCamDist);
            Quaternion q = new Quaternion();
            q.eulerAngles = transform.eulerAngles;
            Matrix4x4 mat = new Matrix4x4();
            mat.SetTRS(Vector3.zero, q, Vector3.one);
            Vector3 p = mat * camOffset;
            transform.position += p;
        }
    }

    float CalcTargetFOV(float distance)
    {
        return 1080 / distance;
    }
}
