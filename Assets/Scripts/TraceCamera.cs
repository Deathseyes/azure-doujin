﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TraceCamera : MonoBehaviour
{
    public float delayTime = 2;
    public Camera traceCamera;

    private bool isTracing = false;
    private GameController controller;
    private float counter = 0;

    // Start is called before the first frame update
    void Start()
    {
        controller = FindObjectOfType<GameController>().GetComponent<GameController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (controller.lastBullet != null)
        {
            transform.position = controller.lastBullet.transform.position;
            counter = delayTime;
        }

        if (Input.GetKeyDown(KeyCode.Z))
            if (!isTracing)
                OpenTracer();
            else
                CloseTracer();
    }

    private void FixedUpdate()
    {
        if (!controller.lastBullet)
        {
            counter -= Time.deltaTime;
            if (counter <= 0) 
                CloseTracer();
        }
    }
    public void OpenTracer()
    {
        if (!isTracing)
        {
            isTracing = true;
            traceCamera.enabled = true;
        }
        
    }

    public void CloseTracer()
    {
        if (isTracing)
        {
            isTracing = false;
            traceCamera.enabled = false;
        }
    }
}
