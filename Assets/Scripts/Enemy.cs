﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class Enemy : MonoBehaviour
{
    public int damage = 1;
    public float life = 1.0f;

    public Transform goal;

    private NavMeshAgent agent;
    private Rigidbody rb;
    private HudText hud;
    private Slider lifeSlider;
    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        rb = GetComponent<Rigidbody>();
        hud = GetComponentInChildren<HudText>();
        lifeSlider = hud.GetComponentInChildren<Slider>();

        lifeSlider.maxValue = life;
        lifeSlider.value = life;

        StartNav();
    }

    // Update is called once per frame
    void Update()
    {
        agent.destination = goal.position;
    }

    //private void OnCollisionEnter(Collision collision)
    //{
    //    if (collision.transform.CompareTag("Bullet"))
    //    {
    //        Destroy(collision.gameObject);
    //    }
    //}

    public void Damage(int damge, float force = 0, Vector3 pos = default(Vector3), float radius = 0)
    {
        hud.HUD(damage);
        life -= damge;
        lifeSlider.value = life;

        if (life <= 0)
        {
            Destroy(gameObject);
        }

        if (force == 0) return;

        agent.isStopped = true;
        rb.isKinematic = false;
        rb.AddExplosionForce(force, pos, radius, 0, ForceMode.Impulse);
        Invoke("StartNav", 2);
    }

    private void StartNav()
    {
        rb.isKinematic = true;
        agent.isStopped = false;
    }
}
