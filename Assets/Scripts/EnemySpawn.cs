﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn : MonoBehaviour
{
    public float speed = 2.0f;
    public Enemy objFile;
    public Transform target;

    private float counter = 0;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        counter += Time.deltaTime;
        if (counter > speed)
        {
            counter = 0;

            Enemy clone = Instantiate(objFile);
            clone.transform.position = transform.position;
            clone.goal = target;
        }
    }
}
