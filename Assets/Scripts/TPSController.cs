﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TPSController : MonoBehaviour
{

    private Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 dir = Vector3.zero;
        dir.z = Input.GetAxis("Vertical");
        dir.x = Input.GetAxis("Horizontal");

        updateAnimation(dir);
    }

    void updateAnimation(Vector3 move)
    {
        animator.SetFloat("Forward", move.z);
        animator.SetFloat("Turn", move.x);
    }
}
