﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float lifeTime = 20.0f; 
    public int damage = 1;
    public GameObject detonator;
    public float explosionLife = 10.0f;
    public float explosionRadius = 50;
    public float explosionForce = 10;

    private Rigidbody rb;

    // Start is called before the first frame update
    void Start()
    {
        Invoke("DestroySelf", lifeTime);
    }

    // Update is called once per frame
    void Update()
    {
    }
    void OnDestroy()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, explosionRadius);
        foreach (Collider hits in colliders)
        {
    
            if (hits.GetComponent<Enemy>())
            {
                hits.GetComponent<Enemy>().Damage(damage, explosionForce, transform.position, explosionRadius);
            }
        }

        GameObject exp = (GameObject) Instantiate(detonator, transform.position, Quaternion.identity);
        Destroy(exp, explosionLife);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Bullet"))
            return;
        Destroy(gameObject);
    }
    void DestroySelf()
    {
        Destroy(gameObject);
    }
}
