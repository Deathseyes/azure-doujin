﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUD : MonoBehaviour
{
    /// <summary>
    /// 滚动速度
    /// </summary>
    public float speed = 1.5f;
    /// <summary>
    /// 销毁时间
    /// </summary>
    public float time = 0.8f;
    /// <summary>
    /// 字体颜色
    /// </summary>
    public Color color= new Color(0.8f, 0.3f, 0.3f, 1);


    private void Start()
    {
        speed = Random.Range(0.5f, speed);
    }

    // Update is called once per frame
    void Update()
    {
        Scroll();
    }

    /// <summary>
    /// 冒泡效果
    /// </summary>
    private void Scroll()
    {
        transform.Translate(Vector3.up * speed * Time.deltaTime);

        //GetComponent<Text>().fontSize--;
        GetComponent<Text>().color = new Color(color.r, color.g, color.b, 1 - Time.deltaTime);
        Destroy(gameObject, time);
    }
}
