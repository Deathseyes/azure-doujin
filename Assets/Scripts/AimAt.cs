﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimAt : MonoBehaviour
{
    public Camera camera;
    public Transform body;
    public Transform[] guns;
    public GameObject[] emiter;
    public GameObject shell;
    public float shellSpeed;
    private GameController controller;

    private Quaternion targetRotation = Quaternion.Euler(0, 0, 45);
    // Start is called before the first frame update
    void Start()
    {
        controller = FindObjectOfType<GameController>().GetComponent<GameController>();
    }

    private void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            for (int i = 0; i < emiter.Length; i++)
            {
                GameObject clone = Instantiate(shell, emiter[i].transform.position, emiter[i].transform.rotation);
                Rigidbody rb = clone.GetComponent<Rigidbody>();
                rb.AddForce(emiter[i].transform.TransformVector(Vector3.forward).normalized * shellSpeed, ForceMode.VelocityChange);
                controller.lastBullet = clone;

                emiter[i].GetComponent<AudioSource>().Play();
            }

        }
    }

    void FixedUpdate()
    {
        Ray ray = camera.ScreenPointToRay(new Vector3(Screen.width/2, Screen.height/2, 0));
        if (Physics.Raycast(ray, out RaycastHit hit))
        {
            Debug.DrawLine(ray.origin, hit.point, Color.green);
            Debug.DrawLine(hit.point, transform.position, Color.yellow);

            Vector3 target = transform.InverseTransformPoint(hit.point);
            target.y = 0;
            body.LookAt(transform.TransformPoint(target), transform.up);
            Debug.DrawLine(hit.point, body.TransformPoint(target), Color.red);

            for (int i = 0; i < guns.Length; i++)
            {
                Vector3 p1 = hit.point;
                Vector3 p2 = guns[i].transform.position;

                float dx = (p1 - p2).magnitude;
                float dy = p1.y - p2.y;

                float a = 5 * dx * dx/(shellSpeed*shellSpeed);
                float b = -dx;
                float c = dy + 5 * dx * dx / (shellSpeed * shellSpeed);

                float x1 = (-b - Mathf.Sqrt(b * b - 4 * a * c))/(2*a);
                float x2 = (-b + Mathf.Sqrt(b * b - 4 * a * c))/(2*a);

                Vector3 aimPoint = hit.point;
                aimPoint.y = guns[i].transform.position.y + dx * x1;

                target = guns[i].InverseTransformPoint(aimPoint);
                target.x = 0;
                guns[i].LookAt(guns[i].TransformPoint(target), transform.up);
                Debug.DrawLine(hit.point, guns[i].TransformPoint(target), Color.blue);
            }
        }
    }
}
