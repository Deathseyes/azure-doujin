﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HudText : MonoBehaviour
{
    /// <summary>
    /// 文字预制体
    /// </summary>
    public GameObject hudText;
    public float damageOffset = 2;



    private void LateUpdate()
    {
        Rotation();  
    }

    /// <summary>
    /// 生成伤害文字
    /// </summary>
    /// <param name="damage"></param>
    public void HUD(int damage)
    {
        GameObject hud = Instantiate(hudText, transform) as GameObject;
        hud.transform.position += transform.right * Random.Range(-damageOffset, damageOffset);
        hud.GetComponent<Text>().text = damage.ToString();
    }

    /// <summary>
    /// 画面始终朝向摄像机
    /// </summary>
    void Rotation()
    {
        transform.rotation = Quaternion.LookRotation(transform.position - Camera.main.transform.position);
    }
}
