﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestShell : MonoBehaviour
{
    public float lifeTime = 10;
    // Start is called before the first frame update
    void Start()
    {
        Invoke("DestroySelf", lifeTime);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void DestroySelf()
    {
        Destroy(gameObject);
    }
}
