﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestAim : MonoBehaviour
{
    public Camera camera;
    public Transform body;
    public Transform[] guns;
    public GameObject[] emiter;
    public GameObject shell;
    public float shellSpeed;
    public float rotateSpeed;

    private Quaternion targetRotation = Quaternion.Euler(0, 0, 45);
    // Start is called before the first frame update
    void Start()
    {
    }

    private void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            for (int i=0; i<emiter.Length; i++)
            {
                GameObject clone = Instantiate(shell, emiter[i].transform.position, emiter[i].transform.rotation);
                Rigidbody rb = clone.GetComponent<Rigidbody>();
                rb.AddForce(emiter[i].transform.TransformVector(Vector3.forward).normalized * shellSpeed, ForceMode.VelocityChange);
            }
        }
    }

    void FixedUpdate()
    {
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime*rotateSpeed);
        if (Quaternion.Angle(targetRotation, transform.rotation) < 1)
        {
            transform.rotation = targetRotation;
            float z = targetRotation.eulerAngles.z * -1;
            targetRotation = Quaternion.Euler(0, 0, z);
        }


        Ray ray = camera.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out RaycastHit hit))
        {
            Debug.DrawLine(ray.origin, hit.point, Color.green);
            Debug.DrawLine(hit.point, transform.position, Color.yellow);

            Vector3 target = body.InverseTransformPoint(hit.point);
            target.y = 0;
            body.LookAt(body.TransformPoint(target), transform.TransformPoint(Vector3.up));
            Debug.DrawLine(hit.point, body.TransformPoint(target), Color.red);

            for (int i=0; i<guns.Length; i++)
            {
                Vector3 p1 = hit.point;
                Vector3 p2 = guns[i].transform.position;

                float dx = (p1 - p2).magnitude;
                float dy = p1.y - p2.y;

                float temp0 = 5 * dx * dx / Mathf.Pow(shellSpeed, 2);
                float temp2 = (dx - Mathf.Sqrt(dx * dx - 4 * temp0 * (temp0 + dy))) / (10 * dx * dx / Mathf.Pow(shellSpeed, 2));
                float theta = Mathf.Atan(temp2) * Mathf.Rad2Deg;

                Vector3 aimPoint = hit.point;
                aimPoint.y = guns[i].transform.position.y + dx * Mathf.Tan(theta);

                target = guns[i].InverseTransformPoint(hit.point);
                target.x = 0;
                guns[i].LookAt(guns[i].TransformPoint(target), transform.TransformPoint(Vector3.up));
                Debug.DrawLine(hit.point, guns[i].TransformPoint(target), Color.blue);
            }
        }
    }
}
