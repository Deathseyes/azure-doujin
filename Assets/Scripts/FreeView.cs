﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreeView : MonoBehaviour
{
    //方向灵敏度  
    public float sensitivityX = 10F;
    public float sensitivityY = 10F;

    //上下最大视角(Y视角)  
    public float minimumY = -60F;
    public float maximumY = 60F;
    
    public float minCamDist = -50;
    public float maxCamDist = -10;

    public float camZoomRate = 10.0f;

    private Camera cam;

    private float rotationY = 0.0f;

    // Start is called before the first frame update
    void Start()
    {
        cam = GetComponentInChildren<Camera>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!cam.enabled)
            return;

        //根据鼠标移动的快慢(增量), 获得相机左右旋转的角度(处理X)  
        float rotationX = Input.GetAxis("Mouse X") * sensitivityX + transform.localEulerAngles.y;
        //根据鼠标移动的快慢(增量), 获得相机上下旋转的角度(处理Y)  
        rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
        //角度限制. rotationY小于min,返回min. 大于max,返回max. 否则返回value   
        rotationY = Mathf.Clamp(rotationY, minimumY, maximumY);
        //总体设置一下相机角度  
        transform.localEulerAngles = new Vector3(-rotationY, rotationX, transform.localEulerAngles.z);

        Vector3 posCam = cam.transform.localPosition;
        posCam.z = Mathf.Clamp(posCam.z + Input.GetAxis("Mouse ScrollWheel") * camZoomRate, minCamDist, maxCamDist);
        cam.transform.localPosition = posCam;
    }
}
